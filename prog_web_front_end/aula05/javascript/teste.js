const verify_parity = () => {
    let number = document.getElementById("number").value;
    let result = document.getElementById("result");

    if (number % 2 == 0) {
        result.textContent = number + ' is pair'
    }
    else {
        result.textContent = number + ' is odd'
    }
}